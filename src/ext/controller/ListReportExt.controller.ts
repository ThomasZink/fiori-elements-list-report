import Event from "sap/ui/base/Event";
import Controller from "sap/ui/core/mvc/Controller"


/**
 * @namespace project1.ext.controller
 */
export default class ExtController extends Controller {
    public customAction(oEvent:Event):void {
        alert('customActionsss' + oEvent.getParameter("id"));
    }
};

/*
sap.ui.controller("project1.ext.controller.ListReportExt",{
    customAction:function(oEvent:Event) {
        alert('customActionsss' + oEvent.getParameter("id"));
    }
});
*/