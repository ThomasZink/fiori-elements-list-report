import AppComponent from "sap/suite/ui/generic/template/lib/AppComponent";

/**
 * @namespace project1
 */
export default class Component extends AppComponent {
	public static metadata = {
		manifest: "json"
	};
}