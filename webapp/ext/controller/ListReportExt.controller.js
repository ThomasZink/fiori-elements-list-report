sap.ui.define(["sap/ui/core/mvc/Controller"], function (Controller) {
  /**
   * @namespace project1.ext.controller
   */
  const ExtController = Controller.extend("project1.ext.controller.ExtController", {
    customAction: function _customAction(oEvent) {
      alert('customActionsss' + oEvent.getParameter("id"));
    }
  });
  ;
  /*
  sap.ui.controller("project1.ext.controller.ListReportExt",{
      customAction:function(oEvent:Event) {
          alert('customActionsss' + oEvent.getParameter("id"));
      }
  });
  */

  return ExtController;
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9leHQvY29udHJvbGxlci9MaXN0UmVwb3J0RXh0LmNvbnRyb2xsZXIudHMiXSwibmFtZXMiOlsiRXh0Q29udHJvbGxlciIsIkNvbnRyb2xsZXIiLCJjdXN0b21BY3Rpb24iLCJvRXZlbnQiLCJhbGVydCIsImdldFBhcmFtZXRlciJdLCJtYXBwaW5ncyI6IjtBQUlBO0FBQ0E7QUFDQTtRQUNxQkEsYSxHQUFzQkMsVTtBQUNoQ0MsSUFBQUEsWSx5QkFBYUMsTSxFQUFtQjtBQUNuQ0MsTUFBQUEsS0FBSyxDQUFDLG9CQUFvQkQsTUFBTSxDQUFDRSxZQUFQLENBQW9CLElBQXBCLENBQXJCLENBQUw7QUFDSDs7QUFDSjtBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztTQVpxQkwsYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBFdmVudCBmcm9tIFwic2FwL3VpL2Jhc2UvRXZlbnRcIjtcclxuaW1wb3J0IENvbnRyb2xsZXIgZnJvbSBcInNhcC91aS9jb3JlL212Yy9Db250cm9sbGVyXCJcclxuXHJcblxyXG4vKipcclxuICogQG5hbWVzcGFjZSBwcm9qZWN0MS5leHQuY29udHJvbGxlclxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXh0Q29udHJvbGxlciBleHRlbmRzIENvbnRyb2xsZXIge1xyXG4gICAgcHVibGljIGN1c3RvbUFjdGlvbihvRXZlbnQ6RXZlbnQpOnZvaWQge1xyXG4gICAgICAgIGFsZXJ0KCdjdXN0b21BY3Rpb25zc3MnICsgb0V2ZW50LmdldFBhcmFtZXRlcihcImlkXCIpKTtcclxuICAgIH1cclxufTtcclxuXHJcbi8qXHJcbnNhcC51aS5jb250cm9sbGVyKFwicHJvamVjdDEuZXh0LmNvbnRyb2xsZXIuTGlzdFJlcG9ydEV4dFwiLHtcclxuICAgIGN1c3RvbUFjdGlvbjpmdW5jdGlvbihvRXZlbnQ6RXZlbnQpIHtcclxuICAgICAgICBhbGVydCgnY3VzdG9tQWN0aW9uc3NzJyArIG9FdmVudC5nZXRQYXJhbWV0ZXIoXCJpZFwiKSk7XHJcbiAgICB9XHJcbn0pO1xyXG4qLyJdfQ==