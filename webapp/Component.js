sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (AppComponent) {
  /**
   * @namespace project1
   */
  const Component = AppComponent.extend("project1.Component", {
    metadata: {
      manifest: "json"
    }
  });
  return Component;
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9Db21wb25lbnQudHMiXSwibmFtZXMiOlsiQ29tcG9uZW50IiwiQXBwQ29tcG9uZW50IiwibWV0YWRhdGEiLCJtYW5pZmVzdCJdLCJtYXBwaW5ncyI6IjtBQUVBO0FBQ0E7QUFDQTtRQUNxQkEsUyxHQUFrQkMsWTtBQUN4QkMsSUFBQUEsUSxFQUFXO0FBQ3hCQyxNQUFBQSxRQUFRLEVBQUU7QUFEYzs7U0FETEgsUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBBcHBDb21wb25lbnQgZnJvbSBcInNhcC9zdWl0ZS91aS9nZW5lcmljL3RlbXBsYXRlL2xpYi9BcHBDb21wb25lbnRcIjtcclxuXHJcbi8qKlxyXG4gKiBAbmFtZXNwYWNlIHByb2plY3QxXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDb21wb25lbnQgZXh0ZW5kcyBBcHBDb21wb25lbnQge1xyXG5cdHB1YmxpYyBzdGF0aWMgbWV0YWRhdGEgPSB7XHJcblx0XHRtYW5pZmVzdDogXCJqc29uXCJcclxuXHR9O1xyXG59Il19