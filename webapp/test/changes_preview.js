//This file used only for loading the changes in the preview and not required to be checked in.
//Load the fake lrep connector only if ui5 version < 1.78
var version = sap.ui.version.split('.');

if (parseInt(version[0]) <= 1 && parseInt(version[1]) < 78) {
  sap.ui.getCore().loadLibraries(['sap/ui/fl']);

  sap.ui.require(['sap/ui/fl/FakeLrepConnector'], function (FakeLrepConnector) {
    jQuery.extend(FakeLrepConnector.prototype, {
      create: function (oChange) {
        return Promise.resolve();
      },
      stringToAscii: function (sCodeAsString) {
        if (!sCodeAsString || sCodeAsString.length === 0) {
          return '';
        }

        var sAsciiString = '';

        for (var i = 0; i < sCodeAsString.length; i++) {
          sAsciiString += sCodeAsString.charCodeAt(i) + ',';
        }

        if (sAsciiString !== null && sAsciiString.length > 0 && sAsciiString.charAt(sAsciiString.length - 1) === ',') {
          sAsciiString = sAsciiString.substring(0, sAsciiString.length - 1);
        }

        return sAsciiString;
      },

      /*
       * Get the content of the sap-ui-cachebuster-info.json file
       * to get the paths to the changes files
       * and get their content
       */
      loadChanges: function () {
        var oResult = {
          changes: [],
          settings: {
            isKeyUser: true,
            isAtoAvailable: false,
            isProductiveSystem: false
          }
        }; //Get the content of the changes folder.

        var aPromises = [];
        var sCacheBusterFilePath = '/sap-ui-cachebuster-info.json';
        var trustedHosts = [/^localhost$/, /^.*.applicationstudio.cloud.sap$/];
        var url = new URL(window.location.toString());
        var isValidHost = trustedHosts.some(host => {
          return host.test(url.hostname);
        });
        /*eslint-disable promise/avoid-new*/

        /*eslint-disable promise/catch-or-return*/

        /*eslint-disable promise/always-return*/

        /*eslint-disable promise/no-nesting*/

        /*eslint-disable consistent-return*/

        /*eslint-disable xss/no-mixed-html*/

        return new Promise(function (resolve, reject) {
          if (!isValidHost) reject(console.log('cannot load flex changes: invalid host'));
          $.ajax({
            url: url.origin + sCacheBusterFilePath,
            type: 'GET',
            cache: false
          }).then(function (oCachebusterContent) {
            //we are looking for only change files
            var aChangeFilesPaths = Object.keys(oCachebusterContent).filter(function (sPath) {
              return sPath.endsWith('.change');
            });
            $.each(aChangeFilesPaths, function (index, sFilePath) {
              //now as we support MTA projects we need to take only changes which are relevant for
              //the current HTML5 module
              //sap-ui-cachebuster-info.json for MTA doesn't start with "webapp/changes" but from <MTA-HTML5-MODULE-NAME>
              //possible change file path patterns
              //webapp/changes/<change-file>
              //<MTA-HTML5-MODULE-NAME>/webapp/changes/<change-file>
              if (sFilePath.indexOf('changes') === 0) {
                /*eslint-disable no-param-reassign*/
                if (!isValidHost) reject(console.log('cannot load flex changes: invalid host'));
                aPromises.push($.ajax({
                  url: url.origin + '/' + sFilePath,
                  type: 'GET',
                  cache: false
                }).then(function (sChangeContent) {
                  return JSON.parse(sChangeContent);
                }));
              }
            });
          }).always(function () {
            return Promise.all(aPromises).then(function (aChanges) {
              return new Promise(function (resolve, reject) {
                // If no changes found, maybe because the app was executed without doing a build.
                // Check for changes folder and load the changes, if any.
                if (aChanges.length === 0) {
                  if (!isValidHost) reject(console.log('cannot load flex changes: invalid host'));
                  $.ajax({
                    url: url.origin + '/changes/',
                    type: 'GET',
                    cache: false
                  }).then(function (sChangesFolderContent) {
                    var regex = /(\/changes\/[^"]*\.change)/g;
                    var result = regex.exec(sChangesFolderContent);

                    while (result !== null) {
                      if (!isValidHost) reject(console.log('cannot load flex changes: invalid host'));
                      aPromises.push($.ajax({
                        url: url.origin + result[1],
                        type: 'GET',
                        cache: false
                      }).then(function (sChangeContent) {
                        return JSON.parse(sChangeContent);
                      }));
                      result = regex.exec(sChangesFolderContent);
                    }

                    resolve(Promise.all(aPromises));
                  }).fail(function (obj) {
                    // No changes folder, then just resolve
                    resolve(aChanges);
                  });
                } else {
                  resolve(aChanges);
                }
              }).then(function (aChanges) {
                var aChangePromises = [],
                    aProcessedChanges = [];
                aChanges.forEach(function (oChange) {
                  var sChangeType = oChange.changeType;

                  if (sChangeType === 'addXML' || sChangeType === 'codeExt') {
                    /*eslint-disable no-nested-ternary*/
                    var sPath = sChangeType === 'addXML' ? oChange.content.fragmentPath : sChangeType === 'codeExt' ? oChange.content.codeRef : '';
                    var sWebappPath = sPath.match(/webapp(.*)/);
                    var sUrl = '/' + sWebappPath[0];
                    aChangePromises.push($.ajax({
                      url: sUrl,
                      type: 'GET',
                      cache: false
                    }).then(function (oFileDocument) {
                      if (sChangeType === 'addXML') {
                        oChange.content.fragment = FakeLrepConnector.prototype.stringToAscii(oFileDocument.documentElement.outerHTML);
                        oChange.content.selectedFragmentContent = oFileDocument.documentElement.outerHTML;
                      } else if (sChangeType === 'codeExt') {
                        oChange.content.code = FakeLrepConnector.prototype.stringToAscii(oFileDocument);
                        oChange.content.extensionControllerContent = oFileDocument;
                      }

                      return oChange;
                    }));
                  } else {
                    aProcessedChanges.push(oChange);
                  }
                }); // aChanges holds the content of all change files from the project (empty array if no such files)
                // sort the array by the creation timestamp of the changes

                if (aChangePromises.length > 0) {
                  return Promise.all(aChangePromises).then(function (aUpdatedChanges) {
                    aUpdatedChanges.forEach(function (oChange) {
                      aProcessedChanges.push(oChange);
                    });
                    aProcessedChanges.sort(function (change1, change2) {
                      return new Date(change1.creation) - new Date(change2.creation);
                    });
                    oResult.changes = aProcessedChanges;
                    var oLrepChange = {
                      changes: oResult,
                      componentClassName: 'project1'
                    };
                    resolve(oLrepChange);
                  });
                } else {
                  aProcessedChanges.sort(function (change1, change2) {
                    return new Date(change1.creation) - new Date(change2.creation);
                  });
                  oResult.changes = aProcessedChanges;
                  var oLrepChange = {
                    changes: oResult,
                    componentClassName: 'project1'
                  };
                  resolve(oLrepChange);
                }
              });
            });
          });
        });
      }
    });
    FakeLrepConnector.enableFakeConnector();
  });
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy90ZXN0L2NoYW5nZXNfcHJldmlldy5qcyJdLCJuYW1lcyI6WyJ2ZXJzaW9uIiwic2FwIiwidWkiLCJzcGxpdCIsInBhcnNlSW50IiwiZ2V0Q29yZSIsImxvYWRMaWJyYXJpZXMiLCJyZXF1aXJlIiwiRmFrZUxyZXBDb25uZWN0b3IiLCJqUXVlcnkiLCJleHRlbmQiLCJwcm90b3R5cGUiLCJjcmVhdGUiLCJvQ2hhbmdlIiwiUHJvbWlzZSIsInJlc29sdmUiLCJzdHJpbmdUb0FzY2lpIiwic0NvZGVBc1N0cmluZyIsImxlbmd0aCIsInNBc2NpaVN0cmluZyIsImkiLCJjaGFyQ29kZUF0IiwiY2hhckF0Iiwic3Vic3RyaW5nIiwibG9hZENoYW5nZXMiLCJvUmVzdWx0IiwiY2hhbmdlcyIsInNldHRpbmdzIiwiaXNLZXlVc2VyIiwiaXNBdG9BdmFpbGFibGUiLCJpc1Byb2R1Y3RpdmVTeXN0ZW0iLCJhUHJvbWlzZXMiLCJzQ2FjaGVCdXN0ZXJGaWxlUGF0aCIsInRydXN0ZWRIb3N0cyIsInVybCIsIlVSTCIsIndpbmRvdyIsImxvY2F0aW9uIiwidG9TdHJpbmciLCJpc1ZhbGlkSG9zdCIsInNvbWUiLCJob3N0IiwidGVzdCIsImhvc3RuYW1lIiwicmVqZWN0IiwiY29uc29sZSIsImxvZyIsIiQiLCJhamF4Iiwib3JpZ2luIiwidHlwZSIsImNhY2hlIiwidGhlbiIsIm9DYWNoZWJ1c3RlckNvbnRlbnQiLCJhQ2hhbmdlRmlsZXNQYXRocyIsIk9iamVjdCIsImtleXMiLCJmaWx0ZXIiLCJzUGF0aCIsImVuZHNXaXRoIiwiZWFjaCIsImluZGV4Iiwic0ZpbGVQYXRoIiwiaW5kZXhPZiIsInB1c2giLCJzQ2hhbmdlQ29udGVudCIsIkpTT04iLCJwYXJzZSIsImFsd2F5cyIsImFsbCIsImFDaGFuZ2VzIiwic0NoYW5nZXNGb2xkZXJDb250ZW50IiwicmVnZXgiLCJyZXN1bHQiLCJleGVjIiwiZmFpbCIsIm9iaiIsImFDaGFuZ2VQcm9taXNlcyIsImFQcm9jZXNzZWRDaGFuZ2VzIiwiZm9yRWFjaCIsInNDaGFuZ2VUeXBlIiwiY2hhbmdlVHlwZSIsImNvbnRlbnQiLCJmcmFnbWVudFBhdGgiLCJjb2RlUmVmIiwic1dlYmFwcFBhdGgiLCJtYXRjaCIsInNVcmwiLCJvRmlsZURvY3VtZW50IiwiZnJhZ21lbnQiLCJkb2N1bWVudEVsZW1lbnQiLCJvdXRlckhUTUwiLCJzZWxlY3RlZEZyYWdtZW50Q29udGVudCIsImNvZGUiLCJleHRlbnNpb25Db250cm9sbGVyQ29udGVudCIsImFVcGRhdGVkQ2hhbmdlcyIsInNvcnQiLCJjaGFuZ2UxIiwiY2hhbmdlMiIsIkRhdGUiLCJjcmVhdGlvbiIsIm9McmVwQ2hhbmdlIiwiY29tcG9uZW50Q2xhc3NOYW1lIiwiZW5hYmxlRmFrZUNvbm5lY3RvciJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBLElBQUlBLE9BQU8sR0FBR0MsR0FBRyxDQUFDQyxFQUFKLENBQU9GLE9BQVAsQ0FBZUcsS0FBZixDQUFxQixHQUFyQixDQUFkOztBQUNBLElBQUlDLFFBQVEsQ0FBQ0osT0FBTyxDQUFDLENBQUQsQ0FBUixDQUFSLElBQXdCLENBQXhCLElBQTZCSSxRQUFRLENBQUNKLE9BQU8sQ0FBQyxDQUFELENBQVIsQ0FBUixHQUF1QixFQUF4RCxFQUE0RDtBQUN4REMsRUFBQUEsR0FBRyxDQUFDQyxFQUFKLENBQU9HLE9BQVAsR0FBaUJDLGFBQWpCLENBQStCLENBQUMsV0FBRCxDQUEvQjs7QUFDQUwsRUFBQUEsR0FBRyxDQUFDQyxFQUFKLENBQU9LLE9BQVAsQ0FBZSxDQUFDLDZCQUFELENBQWYsRUFBZ0QsVUFBU0MsaUJBQVQsRUFBNEI7QUFDeEVDLElBQUFBLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjRixpQkFBaUIsQ0FBQ0csU0FBaEMsRUFBMkM7QUFDdkNDLE1BQUFBLE1BQU0sRUFBRSxVQUFTQyxPQUFULEVBQWtCO0FBQ3RCLGVBQU9DLE9BQU8sQ0FBQ0MsT0FBUixFQUFQO0FBQ0gsT0FIc0M7QUFJdkNDLE1BQUFBLGFBQWEsRUFBRSxVQUFTQyxhQUFULEVBQXdCO0FBQ25DLFlBQUksQ0FBQ0EsYUFBRCxJQUFrQkEsYUFBYSxDQUFDQyxNQUFkLEtBQXlCLENBQS9DLEVBQWtEO0FBQzlDLGlCQUFPLEVBQVA7QUFDSDs7QUFDRCxZQUFJQyxZQUFZLEdBQUcsRUFBbkI7O0FBQ0EsYUFBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxhQUFhLENBQUNDLE1BQWxDLEVBQTBDRSxDQUFDLEVBQTNDLEVBQStDO0FBQzNDRCxVQUFBQSxZQUFZLElBQUlGLGFBQWEsQ0FBQ0ksVUFBZCxDQUF5QkQsQ0FBekIsSUFBOEIsR0FBOUM7QUFDSDs7QUFDRCxZQUNJRCxZQUFZLEtBQUssSUFBakIsSUFDQUEsWUFBWSxDQUFDRCxNQUFiLEdBQXNCLENBRHRCLElBRUFDLFlBQVksQ0FBQ0csTUFBYixDQUFvQkgsWUFBWSxDQUFDRCxNQUFiLEdBQXNCLENBQTFDLE1BQWlELEdBSHJELEVBSUU7QUFDRUMsVUFBQUEsWUFBWSxHQUFHQSxZQUFZLENBQUNJLFNBQWIsQ0FBdUIsQ0FBdkIsRUFBMEJKLFlBQVksQ0FBQ0QsTUFBYixHQUFzQixDQUFoRCxDQUFmO0FBQ0g7O0FBQ0QsZUFBT0MsWUFBUDtBQUNILE9BcEJzQzs7QUFxQnZDO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDWUssTUFBQUEsV0FBVyxFQUFFLFlBQVc7QUFDcEIsWUFBSUMsT0FBTyxHQUFHO0FBQ1ZDLFVBQUFBLE9BQU8sRUFBRSxFQURDO0FBRVZDLFVBQUFBLFFBQVEsRUFBRTtBQUNOQyxZQUFBQSxTQUFTLEVBQUUsSUFETDtBQUVOQyxZQUFBQSxjQUFjLEVBQUUsS0FGVjtBQUdOQyxZQUFBQSxrQkFBa0IsRUFBRTtBQUhkO0FBRkEsU0FBZCxDQURvQixDQVVwQjs7QUFDQSxZQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQSxZQUFJQyxvQkFBb0IsR0FBRywrQkFBM0I7QUFDQSxZQUFJQyxZQUFZLEdBQUcsQ0FBQyxhQUFELEVBQWdCLGtDQUFoQixDQUFuQjtBQUNBLFlBQUlDLEdBQUcsR0FBRyxJQUFJQyxHQUFKLENBQVFDLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkMsUUFBaEIsRUFBUixDQUFWO0FBQ0EsWUFBSUMsV0FBVyxHQUFHTixZQUFZLENBQUNPLElBQWIsQ0FBbUJDLElBQUQsSUFBVTtBQUMxQyxpQkFBT0EsSUFBSSxDQUFDQyxJQUFMLENBQVVSLEdBQUcsQ0FBQ1MsUUFBZCxDQUFQO0FBQ0gsU0FGaUIsQ0FBbEI7QUFHQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQSxlQUFPLElBQUk3QixPQUFKLENBQVksVUFBU0MsT0FBVCxFQUFrQjZCLE1BQWxCLEVBQTBCO0FBQ3pDLGNBQUksQ0FBQ0wsV0FBTCxFQUFrQkssTUFBTSxDQUFDQyxPQUFPLENBQUNDLEdBQVIsQ0FBWSx3Q0FBWixDQUFELENBQU47QUFDbEJDLFVBQUFBLENBQUMsQ0FBQ0MsSUFBRixDQUFPO0FBQ0hkLFlBQUFBLEdBQUcsRUFBRUEsR0FBRyxDQUFDZSxNQUFKLEdBQWFqQixvQkFEZjtBQUVIa0IsWUFBQUEsSUFBSSxFQUFFLEtBRkg7QUFHSEMsWUFBQUEsS0FBSyxFQUFFO0FBSEosV0FBUCxFQUtLQyxJQUxMLENBS1UsVUFBU0MsbUJBQVQsRUFBOEI7QUFDaEM7QUFDQSxnQkFBSUMsaUJBQWlCLEdBQUdDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZSCxtQkFBWixFQUFpQ0ksTUFBakMsQ0FBd0MsVUFBU0MsS0FBVCxFQUFnQjtBQUM1RSxxQkFBT0EsS0FBSyxDQUFDQyxRQUFOLENBQWUsU0FBZixDQUFQO0FBQ0gsYUFGdUIsQ0FBeEI7QUFHQVosWUFBQUEsQ0FBQyxDQUFDYSxJQUFGLENBQU9OLGlCQUFQLEVBQTBCLFVBQVNPLEtBQVQsRUFBZ0JDLFNBQWhCLEVBQTJCO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFJQSxTQUFTLENBQUNDLE9BQVYsQ0FBa0IsU0FBbEIsTUFBaUMsQ0FBckMsRUFBd0M7QUFDcEM7QUFDQSxvQkFBSSxDQUFDeEIsV0FBTCxFQUFrQkssTUFBTSxDQUFDQyxPQUFPLENBQUNDLEdBQVIsQ0FBWSx3Q0FBWixDQUFELENBQU47QUFDbEJmLGdCQUFBQSxTQUFTLENBQUNpQyxJQUFWLENBQ0lqQixDQUFDLENBQUNDLElBQUYsQ0FBTztBQUNIZCxrQkFBQUEsR0FBRyxFQUFFQSxHQUFHLENBQUNlLE1BQUosR0FBYSxHQUFiLEdBQW1CYSxTQURyQjtBQUVIWixrQkFBQUEsSUFBSSxFQUFFLEtBRkg7QUFHSEMsa0JBQUFBLEtBQUssRUFBRTtBQUhKLGlCQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFTYSxjQUFULEVBQXlCO0FBQzdCLHlCQUFPQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0YsY0FBWCxDQUFQO0FBQ0gsaUJBTkQsQ0FESjtBQVNIO0FBQ0osYUFwQkQ7QUFxQkgsV0EvQkwsRUFnQ0tHLE1BaENMLENBZ0NZLFlBQVc7QUFDZixtQkFBT3RELE9BQU8sQ0FBQ3VELEdBQVIsQ0FBWXRDLFNBQVosRUFBdUJxQixJQUF2QixDQUE0QixVQUFTa0IsUUFBVCxFQUFtQjtBQUNsRCxxQkFBTyxJQUFJeEQsT0FBSixDQUFZLFVBQVNDLE9BQVQsRUFBa0I2QixNQUFsQixFQUEwQjtBQUN6QztBQUNBO0FBQ0Esb0JBQUkwQixRQUFRLENBQUNwRCxNQUFULEtBQW9CLENBQXhCLEVBQTJCO0FBQ3ZCLHNCQUFJLENBQUNxQixXQUFMLEVBQWtCSyxNQUFNLENBQUNDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHdDQUFaLENBQUQsQ0FBTjtBQUNsQkMsa0JBQUFBLENBQUMsQ0FBQ0MsSUFBRixDQUFPO0FBQ0hkLG9CQUFBQSxHQUFHLEVBQUVBLEdBQUcsQ0FBQ2UsTUFBSixHQUFhLFdBRGY7QUFFSEMsb0JBQUFBLElBQUksRUFBRSxLQUZIO0FBR0hDLG9CQUFBQSxLQUFLLEVBQUU7QUFISixtQkFBUCxFQUtLQyxJQUxMLENBS1UsVUFBU21CLHFCQUFULEVBQWdDO0FBQ2xDLHdCQUFJQyxLQUFLLEdBQUcsNkJBQVo7QUFDQSx3QkFBSUMsTUFBTSxHQUFHRCxLQUFLLENBQUNFLElBQU4sQ0FBV0gscUJBQVgsQ0FBYjs7QUFFQSwyQkFBT0UsTUFBTSxLQUFLLElBQWxCLEVBQXdCO0FBQ3BCLDBCQUFJLENBQUNsQyxXQUFMLEVBQ0lLLE1BQU0sQ0FBQ0MsT0FBTyxDQUFDQyxHQUFSLENBQVksd0NBQVosQ0FBRCxDQUFOO0FBQ0pmLHNCQUFBQSxTQUFTLENBQUNpQyxJQUFWLENBQ0lqQixDQUFDLENBQUNDLElBQUYsQ0FBTztBQUNIZCx3QkFBQUEsR0FBRyxFQUFFQSxHQUFHLENBQUNlLE1BQUosR0FBYXdCLE1BQU0sQ0FBQyxDQUFELENBRHJCO0FBRUh2Qix3QkFBQUEsSUFBSSxFQUFFLEtBRkg7QUFHSEMsd0JBQUFBLEtBQUssRUFBRTtBQUhKLHVCQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFTYSxjQUFULEVBQXlCO0FBQzdCLCtCQUFPQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0YsY0FBWCxDQUFQO0FBQ0gsdUJBTkQsQ0FESjtBQVNBUSxzQkFBQUEsTUFBTSxHQUFHRCxLQUFLLENBQUNFLElBQU4sQ0FBV0gscUJBQVgsQ0FBVDtBQUNIOztBQUNEeEQsb0JBQUFBLE9BQU8sQ0FBQ0QsT0FBTyxDQUFDdUQsR0FBUixDQUFZdEMsU0FBWixDQUFELENBQVA7QUFDSCxtQkF4QkwsRUF5Qks0QyxJQXpCTCxDQXlCVSxVQUFTQyxHQUFULEVBQWM7QUFDaEI7QUFDQTdELG9CQUFBQSxPQUFPLENBQUN1RCxRQUFELENBQVA7QUFDSCxtQkE1Qkw7QUE2QkgsaUJBL0JELE1BK0JPO0FBQ0h2RCxrQkFBQUEsT0FBTyxDQUFDdUQsUUFBRCxDQUFQO0FBQ0g7QUFDSixlQXJDTSxFQXFDSmxCLElBckNJLENBcUNDLFVBQVNrQixRQUFULEVBQW1CO0FBQ3ZCLG9CQUFJTyxlQUFlLEdBQUcsRUFBdEI7QUFBQSxvQkFDSUMsaUJBQWlCLEdBQUcsRUFEeEI7QUFFQVIsZ0JBQUFBLFFBQVEsQ0FBQ1MsT0FBVCxDQUFpQixVQUFTbEUsT0FBVCxFQUFrQjtBQUMvQixzQkFBSW1FLFdBQVcsR0FBR25FLE9BQU8sQ0FBQ29FLFVBQTFCOztBQUNBLHNCQUFJRCxXQUFXLEtBQUssUUFBaEIsSUFBNEJBLFdBQVcsS0FBSyxTQUFoRCxFQUEyRDtBQUN2RDtBQUNBLHdCQUFJdEIsS0FBSyxHQUNMc0IsV0FBVyxLQUFLLFFBQWhCLEdBQ01uRSxPQUFPLENBQUNxRSxPQUFSLENBQWdCQyxZQUR0QixHQUVNSCxXQUFXLEtBQUssU0FBaEIsR0FDQW5FLE9BQU8sQ0FBQ3FFLE9BQVIsQ0FBZ0JFLE9BRGhCLEdBRUEsRUFMVjtBQU1BLHdCQUFJQyxXQUFXLEdBQUczQixLQUFLLENBQUM0QixLQUFOLENBQVksWUFBWixDQUFsQjtBQUNBLHdCQUFJQyxJQUFJLEdBQUcsTUFBTUYsV0FBVyxDQUFDLENBQUQsQ0FBNUI7QUFDQVIsb0JBQUFBLGVBQWUsQ0FBQ2IsSUFBaEIsQ0FDSWpCLENBQUMsQ0FBQ0MsSUFBRixDQUFPO0FBQ0hkLHNCQUFBQSxHQUFHLEVBQUVxRCxJQURGO0FBRUhyQyxzQkFBQUEsSUFBSSxFQUFFLEtBRkg7QUFHSEMsc0JBQUFBLEtBQUssRUFBRTtBQUhKLHFCQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFTb0MsYUFBVCxFQUF3QjtBQUM1QiwwQkFBSVIsV0FBVyxLQUFLLFFBQXBCLEVBQThCO0FBQzFCbkUsd0JBQUFBLE9BQU8sQ0FBQ3FFLE9BQVIsQ0FBZ0JPLFFBQWhCLEdBQTJCakYsaUJBQWlCLENBQUNHLFNBQWxCLENBQTRCSyxhQUE1QixDQUN2QndFLGFBQWEsQ0FBQ0UsZUFBZCxDQUE4QkMsU0FEUCxDQUEzQjtBQUdBOUUsd0JBQUFBLE9BQU8sQ0FBQ3FFLE9BQVIsQ0FBZ0JVLHVCQUFoQixHQUNJSixhQUFhLENBQUNFLGVBQWQsQ0FBOEJDLFNBRGxDO0FBRUgsdUJBTkQsTUFNTyxJQUFJWCxXQUFXLEtBQUssU0FBcEIsRUFBK0I7QUFDbENuRSx3QkFBQUEsT0FBTyxDQUFDcUUsT0FBUixDQUFnQlcsSUFBaEIsR0FBdUJyRixpQkFBaUIsQ0FBQ0csU0FBbEIsQ0FBNEJLLGFBQTVCLENBQ25Cd0UsYUFEbUIsQ0FBdkI7QUFHQTNFLHdCQUFBQSxPQUFPLENBQUNxRSxPQUFSLENBQWdCWSwwQkFBaEIsR0FBNkNOLGFBQTdDO0FBQ0g7O0FBQ0QsNkJBQU8zRSxPQUFQO0FBQ0gscUJBbEJELENBREo7QUFxQkgsbUJBL0JELE1BK0JPO0FBQ0hpRSxvQkFBQUEsaUJBQWlCLENBQUNkLElBQWxCLENBQXVCbkQsT0FBdkI7QUFDSDtBQUNKLGlCQXBDRCxFQUh1QixDQXdDdkI7QUFDQTs7QUFDQSxvQkFBSWdFLGVBQWUsQ0FBQzNELE1BQWhCLEdBQXlCLENBQTdCLEVBQWdDO0FBQzVCLHlCQUFPSixPQUFPLENBQUN1RCxHQUFSLENBQVlRLGVBQVosRUFBNkJ6QixJQUE3QixDQUFrQyxVQUFTMkMsZUFBVCxFQUEwQjtBQUMvREEsb0JBQUFBLGVBQWUsQ0FBQ2hCLE9BQWhCLENBQXdCLFVBQVNsRSxPQUFULEVBQWtCO0FBQ3RDaUUsc0JBQUFBLGlCQUFpQixDQUFDZCxJQUFsQixDQUF1Qm5ELE9BQXZCO0FBQ0gscUJBRkQ7QUFHQWlFLG9CQUFBQSxpQkFBaUIsQ0FBQ2tCLElBQWxCLENBQXVCLFVBQVNDLE9BQVQsRUFBa0JDLE9BQWxCLEVBQTJCO0FBQzlDLDZCQUFPLElBQUlDLElBQUosQ0FBU0YsT0FBTyxDQUFDRyxRQUFqQixJQUE2QixJQUFJRCxJQUFKLENBQVNELE9BQU8sQ0FBQ0UsUUFBakIsQ0FBcEM7QUFDSCxxQkFGRDtBQUdBM0Usb0JBQUFBLE9BQU8sQ0FBQ0MsT0FBUixHQUFrQm9ELGlCQUFsQjtBQUNBLHdCQUFJdUIsV0FBVyxHQUFHO0FBQ2QzRSxzQkFBQUEsT0FBTyxFQUFFRCxPQURLO0FBRWQ2RSxzQkFBQUEsa0JBQWtCLEVBQUU7QUFGTixxQkFBbEI7QUFJQXZGLG9CQUFBQSxPQUFPLENBQUNzRixXQUFELENBQVA7QUFDSCxtQkFiTSxDQUFQO0FBY0gsaUJBZkQsTUFlTztBQUNIdkIsa0JBQUFBLGlCQUFpQixDQUFDa0IsSUFBbEIsQ0FBdUIsVUFBU0MsT0FBVCxFQUFrQkMsT0FBbEIsRUFBMkI7QUFDOUMsMkJBQU8sSUFBSUMsSUFBSixDQUFTRixPQUFPLENBQUNHLFFBQWpCLElBQTZCLElBQUlELElBQUosQ0FBU0QsT0FBTyxDQUFDRSxRQUFqQixDQUFwQztBQUNILG1CQUZEO0FBR0EzRSxrQkFBQUEsT0FBTyxDQUFDQyxPQUFSLEdBQWtCb0QsaUJBQWxCO0FBQ0Esc0JBQUl1QixXQUFXLEdBQUc7QUFDZDNFLG9CQUFBQSxPQUFPLEVBQUVELE9BREs7QUFFZDZFLG9CQUFBQSxrQkFBa0IsRUFBRTtBQUZOLG1CQUFsQjtBQUlBdkYsa0JBQUFBLE9BQU8sQ0FBQ3NGLFdBQUQsQ0FBUDtBQUNIO0FBQ0osZUF6R00sQ0FBUDtBQTBHSCxhQTNHTSxDQUFQO0FBNEdILFdBN0lMO0FBOElILFNBaEpNLENBQVA7QUFpSkg7QUFuTXNDLEtBQTNDO0FBcU1BN0YsSUFBQUEsaUJBQWlCLENBQUMrRixtQkFBbEI7QUFDSCxHQXZNRDtBQXdNSCIsInNvdXJjZXNDb250ZW50IjpbIi8vVGhpcyBmaWxlIHVzZWQgb25seSBmb3IgbG9hZGluZyB0aGUgY2hhbmdlcyBpbiB0aGUgcHJldmlldyBhbmQgbm90IHJlcXVpcmVkIHRvIGJlIGNoZWNrZWQgaW4uXG4vL0xvYWQgdGhlIGZha2UgbHJlcCBjb25uZWN0b3Igb25seSBpZiB1aTUgdmVyc2lvbiA8IDEuNzhcbnZhciB2ZXJzaW9uID0gc2FwLnVpLnZlcnNpb24uc3BsaXQoJy4nKTtcbmlmIChwYXJzZUludCh2ZXJzaW9uWzBdKSA8PSAxICYmIHBhcnNlSW50KHZlcnNpb25bMV0pIDwgNzgpIHtcbiAgICBzYXAudWkuZ2V0Q29yZSgpLmxvYWRMaWJyYXJpZXMoWydzYXAvdWkvZmwnXSk7XG4gICAgc2FwLnVpLnJlcXVpcmUoWydzYXAvdWkvZmwvRmFrZUxyZXBDb25uZWN0b3InXSwgZnVuY3Rpb24oRmFrZUxyZXBDb25uZWN0b3IpIHtcbiAgICAgICAgalF1ZXJ5LmV4dGVuZChGYWtlTHJlcENvbm5lY3Rvci5wcm90b3R5cGUsIHtcbiAgICAgICAgICAgIGNyZWF0ZTogZnVuY3Rpb24ob0NoYW5nZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBzdHJpbmdUb0FzY2lpOiBmdW5jdGlvbihzQ29kZUFzU3RyaW5nKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFzQ29kZUFzU3RyaW5nIHx8IHNDb2RlQXNTdHJpbmcubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIHNBc2NpaVN0cmluZyA9ICcnO1xuICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc0NvZGVBc1N0cmluZy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBzQXNjaWlTdHJpbmcgKz0gc0NvZGVBc1N0cmluZy5jaGFyQ29kZUF0KGkpICsgJywnO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgIHNBc2NpaVN0cmluZyAhPT0gbnVsbCAmJlxuICAgICAgICAgICAgICAgICAgICBzQXNjaWlTdHJpbmcubGVuZ3RoID4gMCAmJlxuICAgICAgICAgICAgICAgICAgICBzQXNjaWlTdHJpbmcuY2hhckF0KHNBc2NpaVN0cmluZy5sZW5ndGggLSAxKSA9PT0gJywnXG4gICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgIHNBc2NpaVN0cmluZyA9IHNBc2NpaVN0cmluZy5zdWJzdHJpbmcoMCwgc0FzY2lpU3RyaW5nLmxlbmd0aCAtIDEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gc0FzY2lpU3RyaW5nO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgKiBHZXQgdGhlIGNvbnRlbnQgb2YgdGhlIHNhcC11aS1jYWNoZWJ1c3Rlci1pbmZvLmpzb24gZmlsZVxuICAgICAgICAgICAgICogdG8gZ2V0IHRoZSBwYXRocyB0byB0aGUgY2hhbmdlcyBmaWxlc1xuICAgICAgICAgICAgICogYW5kIGdldCB0aGVpciBjb250ZW50XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGxvYWRDaGFuZ2VzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgb1Jlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgY2hhbmdlczogW10sXG4gICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpc0tleVVzZXI6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0F0b0F2YWlsYWJsZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgICAgICBpc1Byb2R1Y3RpdmVTeXN0ZW06IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgLy9HZXQgdGhlIGNvbnRlbnQgb2YgdGhlIGNoYW5nZXMgZm9sZGVyLlxuICAgICAgICAgICAgICAgIHZhciBhUHJvbWlzZXMgPSBbXTtcbiAgICAgICAgICAgICAgICB2YXIgc0NhY2hlQnVzdGVyRmlsZVBhdGggPSAnL3NhcC11aS1jYWNoZWJ1c3Rlci1pbmZvLmpzb24nO1xuICAgICAgICAgICAgICAgIHZhciB0cnVzdGVkSG9zdHMgPSBbL15sb2NhbGhvc3QkLywgL14uKi5hcHBsaWNhdGlvbnN0dWRpby5jbG91ZC5zYXAkL107XG4gICAgICAgICAgICAgICAgdmFyIHVybCA9IG5ldyBVUkwod2luZG93LmxvY2F0aW9uLnRvU3RyaW5nKCkpO1xuICAgICAgICAgICAgICAgIHZhciBpc1ZhbGlkSG9zdCA9IHRydXN0ZWRIb3N0cy5zb21lKChob3N0KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBob3N0LnRlc3QodXJsLmhvc3RuYW1lKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAvKmVzbGludC1kaXNhYmxlIHByb21pc2UvYXZvaWQtbmV3Ki9cbiAgICAgICAgICAgICAgICAvKmVzbGludC1kaXNhYmxlIHByb21pc2UvY2F0Y2gtb3ItcmV0dXJuKi9cbiAgICAgICAgICAgICAgICAvKmVzbGludC1kaXNhYmxlIHByb21pc2UvYWx3YXlzLXJldHVybiovXG4gICAgICAgICAgICAgICAgLyplc2xpbnQtZGlzYWJsZSBwcm9taXNlL25vLW5lc3RpbmcqL1xuICAgICAgICAgICAgICAgIC8qZXNsaW50LWRpc2FibGUgY29uc2lzdGVudC1yZXR1cm4qL1xuICAgICAgICAgICAgICAgIC8qZXNsaW50LWRpc2FibGUgeHNzL25vLW1peGVkLWh0bWwqL1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpc1ZhbGlkSG9zdCkgcmVqZWN0KGNvbnNvbGUubG9nKCdjYW5ub3QgbG9hZCBmbGV4IGNoYW5nZXM6IGludmFsaWQgaG9zdCcpKTtcbiAgICAgICAgICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybDogdXJsLm9yaWdpbiArIHNDYWNoZUJ1c3RlckZpbGVQYXRoLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWNoZTogZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKG9DYWNoZWJ1c3RlckNvbnRlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL3dlIGFyZSBsb29raW5nIGZvciBvbmx5IGNoYW5nZSBmaWxlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBhQ2hhbmdlRmlsZXNQYXRocyA9IE9iamVjdC5rZXlzKG9DYWNoZWJ1c3RlckNvbnRlbnQpLmZpbHRlcihmdW5jdGlvbihzUGF0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc1BhdGguZW5kc1dpdGgoJy5jaGFuZ2UnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkLmVhY2goYUNoYW5nZUZpbGVzUGF0aHMsIGZ1bmN0aW9uKGluZGV4LCBzRmlsZVBhdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9ub3cgYXMgd2Ugc3VwcG9ydCBNVEEgcHJvamVjdHMgd2UgbmVlZCB0byB0YWtlIG9ubHkgY2hhbmdlcyB3aGljaCBhcmUgcmVsZXZhbnQgZm9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vdGhlIGN1cnJlbnQgSFRNTDUgbW9kdWxlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vc2FwLXVpLWNhY2hlYnVzdGVyLWluZm8uanNvbiBmb3IgTVRBIGRvZXNuJ3Qgc3RhcnQgd2l0aCBcIndlYmFwcC9jaGFuZ2VzXCIgYnV0IGZyb20gPE1UQS1IVE1MNS1NT0RVTEUtTkFNRT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9wb3NzaWJsZSBjaGFuZ2UgZmlsZSBwYXRoIHBhdHRlcm5zXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vd2ViYXBwL2NoYW5nZXMvPGNoYW5nZS1maWxlPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLzxNVEEtSFRNTDUtTU9EVUxFLU5BTUU+L3dlYmFwcC9jaGFuZ2VzLzxjaGFuZ2UtZmlsZT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNGaWxlUGF0aC5pbmRleE9mKCdjaGFuZ2VzJykgPT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24qL1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc1ZhbGlkSG9zdCkgcmVqZWN0KGNvbnNvbGUubG9nKCdjYW5ub3QgbG9hZCBmbGV4IGNoYW5nZXM6IGludmFsaWQgaG9zdCcpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFQcm9taXNlcy5wdXNoKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogdXJsLm9yaWdpbiArICcvJyArIHNGaWxlUGF0aCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhY2hlOiBmYWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24oc0NoYW5nZUNvbnRlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEpTT04ucGFyc2Uoc0NoYW5nZUNvbnRlbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmFsd2F5cyhmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5hbGwoYVByb21pc2VzKS50aGVuKGZ1bmN0aW9uKGFDaGFuZ2VzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIElmIG5vIGNoYW5nZXMgZm91bmQsIG1heWJlIGJlY2F1c2UgdGhlIGFwcCB3YXMgZXhlY3V0ZWQgd2l0aG91dCBkb2luZyBhIGJ1aWxkLlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gQ2hlY2sgZm9yIGNoYW5nZXMgZm9sZGVyIGFuZCBsb2FkIHRoZSBjaGFuZ2VzLCBpZiBhbnkuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYUNoYW5nZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFpc1ZhbGlkSG9zdCkgcmVqZWN0KGNvbnNvbGUubG9nKCdjYW5ub3QgbG9hZCBmbGV4IGNoYW5nZXM6IGludmFsaWQgaG9zdCcpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHVybC5vcmlnaW4gKyAnL2NoYW5nZXMvJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhY2hlOiBmYWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKHNDaGFuZ2VzRm9sZGVyQ29udGVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJlZ2V4ID0gLyhcXC9jaGFuZ2VzXFwvW15cIl0qXFwuY2hhbmdlKS9nO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHJlc3VsdCA9IHJlZ2V4LmV4ZWMoc0NoYW5nZXNGb2xkZXJDb250ZW50KTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2hpbGUgKHJlc3VsdCAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghaXNWYWxpZEhvc3QpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChjb25zb2xlLmxvZygnY2Fubm90IGxvYWQgZmxleCBjaGFuZ2VzOiBpbnZhbGlkIGhvc3QnKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYVByb21pc2VzLnB1c2goXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHVybC5vcmlnaW4gKyByZXN1bHRbMV0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnR0VUJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhY2hlOiBmYWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uKHNDaGFuZ2VDb250ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShzQ2hhbmdlQ29udGVudCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSByZWdleC5leGVjKHNDaGFuZ2VzRm9sZGVyQ29udGVudCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKFByb21pc2UuYWxsKGFQcm9taXNlcykpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZmFpbChmdW5jdGlvbihvYmopIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIE5vIGNoYW5nZXMgZm9sZGVyLCB0aGVuIGp1c3QgcmVzb2x2ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShhQ2hhbmdlcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGFDaGFuZ2VzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbihhQ2hhbmdlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGFDaGFuZ2VQcm9taXNlcyA9IFtdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFQcm9jZXNzZWRDaGFuZ2VzID0gW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhQ2hhbmdlcy5mb3JFYWNoKGZ1bmN0aW9uKG9DaGFuZ2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgc0NoYW5nZVR5cGUgPSBvQ2hhbmdlLmNoYW5nZVR5cGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNDaGFuZ2VUeXBlID09PSAnYWRkWE1MJyB8fCBzQ2hhbmdlVHlwZSA9PT0gJ2NvZGVFeHQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qZXNsaW50LWRpc2FibGUgbm8tbmVzdGVkLXRlcm5hcnkqL1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgc1BhdGggPVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc0NoYW5nZVR5cGUgPT09ICdhZGRYTUwnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBvQ2hhbmdlLmNvbnRlbnQuZnJhZ21lbnRQYXRoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBzQ2hhbmdlVHlwZSA9PT0gJ2NvZGVFeHQnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBvQ2hhbmdlLmNvbnRlbnQuY29kZVJlZlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzV2ViYXBwUGF0aCA9IHNQYXRoLm1hdGNoKC93ZWJhcHAoLiopLyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBzVXJsID0gJy8nICsgc1dlYmFwcFBhdGhbMF07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFDaGFuZ2VQcm9taXNlcy5wdXNoKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHNVcmwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FjaGU6IGZhbHNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uKG9GaWxlRG9jdW1lbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc0NoYW5nZVR5cGUgPT09ICdhZGRYTUwnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9DaGFuZ2UuY29udGVudC5mcmFnbWVudCA9IEZha2VMcmVwQ29ubmVjdG9yLnByb3RvdHlwZS5zdHJpbmdUb0FzY2lpKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb0ZpbGVEb2N1bWVudC5kb2N1bWVudEVsZW1lbnQub3V0ZXJIVE1MXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9DaGFuZ2UuY29udGVudC5zZWxlY3RlZEZyYWdtZW50Q29udGVudCA9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvRmlsZURvY3VtZW50LmRvY3VtZW50RWxlbWVudC5vdXRlckhUTUw7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzQ2hhbmdlVHlwZSA9PT0gJ2NvZGVFeHQnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9DaGFuZ2UuY29udGVudC5jb2RlID0gRmFrZUxyZXBDb25uZWN0b3IucHJvdG90eXBlLnN0cmluZ1RvQXNjaWkoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvRmlsZURvY3VtZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9DaGFuZ2UuY29udGVudC5leHRlbnNpb25Db250cm9sbGVyQ29udGVudCA9IG9GaWxlRG9jdW1lbnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBvQ2hhbmdlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhUHJvY2Vzc2VkQ2hhbmdlcy5wdXNoKG9DaGFuZ2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gYUNoYW5nZXMgaG9sZHMgdGhlIGNvbnRlbnQgb2YgYWxsIGNoYW5nZSBmaWxlcyBmcm9tIHRoZSBwcm9qZWN0IChlbXB0eSBhcnJheSBpZiBubyBzdWNoIGZpbGVzKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gc29ydCB0aGUgYXJyYXkgYnkgdGhlIGNyZWF0aW9uIHRpbWVzdGFtcCBvZiB0aGUgY2hhbmdlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFDaGFuZ2VQcm9taXNlcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UuYWxsKGFDaGFuZ2VQcm9taXNlcykudGhlbihmdW5jdGlvbihhVXBkYXRlZENoYW5nZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYVVwZGF0ZWRDaGFuZ2VzLmZvckVhY2goZnVuY3Rpb24ob0NoYW5nZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYVByb2Nlc3NlZENoYW5nZXMucHVzaChvQ2hhbmdlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFQcm9jZXNzZWRDaGFuZ2VzLnNvcnQoZnVuY3Rpb24oY2hhbmdlMSwgY2hhbmdlMikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBEYXRlKGNoYW5nZTEuY3JlYXRpb24pIC0gbmV3IERhdGUoY2hhbmdlMi5jcmVhdGlvbik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvUmVzdWx0LmNoYW5nZXMgPSBhUHJvY2Vzc2VkQ2hhbmdlcztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9McmVwQ2hhbmdlID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlczogb1Jlc3VsdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudENsYXNzTmFtZTogJ3Byb2plY3QxJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG9McmVwQ2hhbmdlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYVByb2Nlc3NlZENoYW5nZXMuc29ydChmdW5jdGlvbihjaGFuZ2UxLCBjaGFuZ2UyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgRGF0ZShjaGFuZ2UxLmNyZWF0aW9uKSAtIG5ldyBEYXRlKGNoYW5nZTIuY3JlYXRpb24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9SZXN1bHQuY2hhbmdlcyA9IGFQcm9jZXNzZWRDaGFuZ2VzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBvTHJlcENoYW5nZSA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlczogb1Jlc3VsdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50Q2xhc3NOYW1lOiAncHJvamVjdDEnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG9McmVwQ2hhbmdlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIEZha2VMcmVwQ29ubmVjdG9yLmVuYWJsZUZha2VDb25uZWN0b3IoKTtcbiAgICB9KTtcbn1cbiJdfQ==